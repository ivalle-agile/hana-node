-- GET TABLES --
SELECT SCHEMA_NAME, LEFT(TABLE_NAME, LOCATE(TABLE_NAME, '::')-1) as COMPONENT,RIGHT(TABLE_NAME, LENGTH(TABLE_NAME)-LOCATE(TABLE_NAME, '::')-1) AS TABLE_NAME
FROM M_CS_TABLES
WHERE SCHEMA_NAME = 'TIMP'
AND TABLE_NAME LIKE 'BCB::%'
ORDER BY COMPONENT, TABLE_NAME ASC;


-- GET TABLE COLUMNS --
-- SELECT COLUMN_NAME, POSITION, LENGTH, SCALE, IS_NULLABLE, CS_DATA_TYPE_NAME, TABLE_NAME
-- FROM TABLE_COLUMNS
-- WHERE SCHEMA_NAME = 'TIMP'
-- AND TABLE_NAME = 'BCB::BuilderConfigurationEEF'
-- ORDER BY POSITION

-- SELECT bc.id as bcid, bce.id as bceid
-- FROM
-- "TIMP"."BCB::BuilderConfiguration" bc,
-- "TIMP"."BCB::BuilderConfigurationTax" bce

-- WHERE bc.id = bce.ID_BUILDER_CONFIGURATION
-- AND bc.id = 220

-- JOINS --
-- SELECT 
--     bc.id as C_ID,
--     hc.name as H_ID, 
--     be.ID as be_id,
--     bev.id as bev_id,
--     op.ID as O_ID,
--     op.key as O_KEY,
--     op.name as O_NAME,
--     opvl.id AS OV_ID,
--     opvl.value AS O_VAL
-- FROM 
-- "TIMP"."BCB::BuilderConfiguration" bc,
-- "TIMP"."BCB::HierarchyConfiguration" hc,
-- "TIMP"."BCB::BuilderExecution" be,
-- "TIMP"."BCB::BuilderExecutionVersion" bev,
-- "TIMP"."BCB::Output" op,
-- "TIMP"."BCB::OutputValue" opvl


-- WHERE 1=1 AND
-- --joins
-- hc.ID = bc.ID_HIERARCHY AND

-- bc.ID = be.ID_CONFIGURATION AND
-- be.ID = bev.ID_EXECUTION AND

-- bc.ID = op.ID_CONFIGURATION AND
-- op.ID = opvl.ID_OUTPUT AND


-- bc.STATUS = 0

-- ORDER BY bc.ID;


-- SELECT op.ID, opvl.ID as VAl_ID, opvl.ID_COMPANY, opvl.UF, opvl.ID_BRANCH, opvl.TAX_CODE , opvl.YEAR , opvl.MONTH , opvl.SUBPERIOD , opvl.VALUE 
-- FROM
-- "TIMP"."BCB::Output" op,
-- "TIMP"."BCB::OutputValue" opvl
-- WHERE op.ID = 268 and opvl.ID_OUTPUT = 268;